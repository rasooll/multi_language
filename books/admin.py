# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Book
from django.utils.translation import ugettext_lazy as _


class BookAdmin(admin.ModelAdmin):
    fieldsets = (
        (_('Persian'), {
            # 'classes': ('collapse',),
            'fields': (('name', 'author'), 'des')
        }),
        ('English', {
            # 'classes': ('collapse',),
            'fields': (('name_en', 'author_en'), 'des_en')
        })
    )


admin.site.register(Book, BookAdmin)
