# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _ , get_language

class Book(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("name"))
    des = models.TextField(verbose_name=_("description"))
    author = models.CharField(max_length=50, verbose_name=_("author"))
    name_en = models.CharField(max_length=50, verbose_name="name")
    des_en = models.TextField(verbose_name="description")
    author_en = models.CharField(max_length=50, verbose_name="author")

    def __str__(self):
        if get_language() == 'en':
            return self.name_en
        return self.name
