from django.urls import path
# from django.conf.urls.i18n import i18n_patterns
from .views import home


urlpatterns = [
    path('', home, name="home"),
]

